@startuml
title 4 assignTask
actor Actor
participant "CatERingAppManager.sheetManager:SheetManager" as sm
participant "activity:Activity" as aa
participant "activity.tasks: List<Task>" as at
participant "task:Task" as tt
participant "r:SheetEventReceiver" as r

Actor -> sm ++: assignTask(activity,shift,time?,quantity?,cook?,to_be_prepared?,completed?)

alt currentSheet == null || \n !currentSheet.hasActivity(activity) 
  sm -->> Actor: throw UseCaseLogicException
else
  sm -> aa++: addTask(shift,time?,quantity?,cook?,to_be_prepared?,completed?)
  
  aa --> tt**: create(shift,time?,quantity?,cook?,to_be_prepared?,completed?)
  activate tt

  tt -> tt: setShift(shift)
  alt time == null
    /'
    default value = 0
    '/
    tt -> tt: setTime(0)
  else
    tt -> tt: setTime(time)
  end
  alt quantity == null
    /'
    default value = 0
    '/
    tt -> tt: setQuantity(0)
  else
    tt -> tt: setQuantity(quantity)
  end
  opt cook != null
    /'
    default value = ""
    '/
    tt -> tt: setCook(cook)
  end
  alt to_be_prepared == null
    /'
    default value = no
    '/
    tt -> tt: setToBePrepared(false)
  else
    tt -> tt: setToBePrepared(to_be_prepared)
  end
  alt completed == null
    /'
    default value = no
    '/
    tt -> tt: setCompleted(false)
  else
    tt -> tt: setCompleted(completed)
  end
  deactivate tt
  aa -> at ++: add(task)
  deactivate at
  aa -->> sm: task
  deactivate aa
  
  group for each r in Receiver
  	sm -> r: notifyAssignTask(currentSheet,activity,task)
  end
  sm -->> Actor--: task
end

@enduml

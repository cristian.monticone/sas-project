#!/bin/sh

# Install a post-merge git hook with the current version of auto_plantuml.sh

BASE_DIR="$(git rev-parse --show-toplevel)"

cp $BASE_DIR/scripts/auto_plantuml.sh $BASE_DIR/.git/hooks/post-merge
cp $BASE_DIR/scripts/auto_plantuml.sh $BASE_DIR/.git/hooks/post-commit

chmod +x $BASE_DIR/.git/hooks/post-merge

echo "Configured a post-merge git hook into this repository"

#!/bin/sh

# A shell script that runs plantuml on changed .uml files in all the repo.

BASE_DIR="$(git rev-parse --show-toplevel)"

EXTENSION="png"

if [ "$1" = "svg" ]; then
  SVG_OPTION="-tsvg "
  EXTENSION="svg"
elif [ "$1" = "tex" ]; then
  SVG_OPTION="-tsvg "
  EXTENSION="pdf_tex"
fi

echo "Checking for uml file changes to recompile via plantuml..."

file_recompiled=0

for uml_file in $(find $BASE_DIR -type f -name "*.uml"); do
  png_file=${uml_file%.uml}.$EXTENSION

  uml_file_mtime=$(date +%s -r "$uml_file")

  if [ -f "$png_file" ]; then
    png_file_mtime=$(date +%s -r "$png_file")
  else
    png_file_mtime=0
  fi
  
  if [ "$uml_file_mtime" -gt "$png_file_mtime" ]; then
    echo "$uml_file seems to be outdated! I'm recompiling it for you..."
    plantuml $SVG_OPTION "$uml_file" 

    if [ "$1" = "tex" ]; then
      echo "Exporting svg into latex pdf..."
      inkscape --without-gui --file="${png_file%pdf_tex}svg" --export-pdf="${png_file%_tex}" --export-latex
    fi

    file_recompiled=$(($file_recompiled+1))
  fi

done

if [ "$file_recompiled" -gt "0" ]; then
  echo "All right! $file_recompiled uml files recompiled correctly! :)"
else
  echo "No uml files has been changed, nothing to do here. :("
fi

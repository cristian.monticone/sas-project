/'
 ' This DSD implements the single role assignment for a previously selected
 ' event.
 '/
@startuml
title 5 assignRole
actor Actor
participant "CatERingAppManager.eventManager:EventManager" as em
participant "CatERingAppManager.event:Event" as e
participant "assignment:Assignment" as assgn
participant "r:EventReceiver" as r

Actor -> em++: assignRole(staff, roleText)
alt currentEvent == null
	em -->> Actor: throw UseCaseLogicException
else !staff.isStaff()
	em -->> Actor: throw UseCaseLogicException
else
    em -> e++: getRoles()
    e -->> em: roles
    deactivate e

    group for each r in roles
        opt r.getInvolvedUser() == staff
          em -> assgn++: setDescription(roleText)
          deactivate assgn
        end
    end 

    group for each r in receivers
    	em -> r++:notifyRoleAssigned(currentEvent, assignment)
        deactivate r
    end 

    em -->> Actor --
end
@enduml

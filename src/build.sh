#!/bin/sh

OUTPUD_DIR="build"
HARD_REBULD="yes"

if [ ! -d "libs" ]; then
  echo "No libs folder found. Launch setup.sh script..."
  ./setup.sh
fi

if [ "$HARD_REBULD" = "yes" ]; then
  echo "Hard rebuilding started."
  rm -rf $OUTPUD_DIR
else
  echo "Soft rebuilding started."
fi

if [ ! -d "$OUTPUD_DIR" ]; then
  mkdir $OUTPUD_DIR
  mkdir $OUTPUD_DIR/catering
fi

cp catering/*fxml $OUTPUD_DIR/catering/

javac -Xlint:deprecation -Xlint:unchecked -d $OUTPUD_DIR --module-path libs --add-modules javafx.fxml,javafx.controls catering/businesslogic/*java catering/persistence/*java catering/*java

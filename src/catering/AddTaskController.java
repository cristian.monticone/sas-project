package catering;

import catering.businesslogic.*;
import catering.businesslogic.Menu;
import catering.businesslogic.MenuItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;

public class AddTaskController {
    @FXML
    private ChoiceBox<Shift> shiftsBox;

    @FXML
    private ChoiceBox<User> cooksBox;

    @FXML
    private CheckBox toBePrepared;

    @FXML
    private CheckBox completed;

    @FXML
    private TextField quantity;

    @FXML
    private TextField time;

    @FXML
    private Label title;

    @FXML
    private Button assignButton;

    private Activity selectedActivity;
    private Task selectedTask;

    private ObservableList<Shift> observableShifts;
    private ObservableList<User> observableCooks;

    public AddTaskController() {

    }

    public void setSelectedActivity(Activity a) {
        this.selectedActivity = a;
        title.setText("Inserisci compito per l'attività " + selectedActivity);
    }

    public void setSelectedTask(Task t) {
        this.selectedTask = t;
        title.setText("Modifica compito dell'attività " + selectedActivity);
        toBePrepared.setSelected(t.isToBePrepared());
        completed.setSelected(t.isCompleted());
        shiftsBox.getSelectionModel().select(t.getShift());
        cooksBox.getSelectionModel().select(t.getCook());
        time.setText(Long.toString(t.getTime().toMinutes()));
        quantity.setText(Float.toString(t.getQuantity()));
        assignButton.setText("Modifica");
    }

    public void initialize() {
        List<Shift> shifts = CateringAppManager.shiftManager.getShiftTable();
        observableShifts = FXCollections.observableList(shifts);
        shiftsBox.setItems(observableShifts);
        shiftsBox.getSelectionModel().selectFirst();

        List<User> users = CateringAppManager.userManager.getAllUsers();
        observableCooks = FXCollections.observableList(users);
        cooksBox.setItems(observableCooks);
        cooksBox.getSelectionModel().selectFirst();

        toBePrepared.setAllowIndeterminate(false);
        toBePrepared.setSelected(true);

        completed.setAllowIndeterminate(false);
        completed.setSelected(false);

        quantity.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    quantity.setText(oldValue);
                }
            }
        });

        time.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}?")) {
                    time.setText(oldValue);
                }
            }
        });
    }

    @FXML
    public void exitButton(ActionEvent e) {
        ((Stage) (((Button) e.getSource()).getScene().getWindow())).close();
    }

    @FXML
    public void onAssign(ActionEvent e) {
        Shift shift = shiftsBox.getSelectionModel().getSelectedItem();
        User cook = cooksBox.getSelectionModel().getSelectedItem();

        Duration d = null;
        if (!time.getCharacters().toString().equals(""))
          d = Duration.of(Long.valueOf(time.getCharacters().toString()), ChronoUnit.MINUTES);

        Float f = null;
        if (!quantity.getCharacters().toString().equals(""))
          f = Float.valueOf(quantity.getCharacters().toString());

        if (selectedTask == null)
          CateringAppManager.sheetManager.assignTask(selectedActivity, shift, d, f,
                           cook, toBePrepared.isSelected(), completed.isSelected());
        else
          CateringAppManager.sheetManager.modifyTask(selectedActivity, selectedTask, shift, d, f,
                           cook, toBePrepared.isSelected(), completed.isSelected());

        ((Stage) (((Button) e.getSource()).getScene().getWindow())).close();
    }
}

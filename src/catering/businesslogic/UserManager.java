package catering.businesslogic;

import catering.persistence.DataManager;
import java.util.List;
import java.util.ArrayList;

public class UserManager {

    private User currentUser;
    private List<User> userList;

    public void initialize() {
        // Questa è una versione "mockup" di UserManager
        // Ossia una versione semplificata che ha lo scopo di testare l'applicazione
        // Per questa ragione il metodo initialize() carica un utente di default dal DB
        this.currentUser = CateringAppManager.dataManager.loadUser("Viola");
        this.userList = CateringAppManager.dataManager.loadUsers();
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public List<User> getAllUsers() {
        List<User> result = new ArrayList<>();
        result.addAll(userList);
        return result;
    }
}

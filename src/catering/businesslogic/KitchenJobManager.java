package catering.businesslogic;

import java.util.ArrayList;
import java.util.List;

public class KitchenJobManager {

    private List<KitchenJob> kitchenJobs;

    public KitchenJobManager() {
    }

    // Nota: nell'inizializzazione non carichiamo l'elenco di ricette
    // perché lo faremo "onDemand", ossia se viene richiesto da qualche altro oggetto
    // L'idea è evitare di caricare tutto se non serve.
    public void initialize() {
    }

    public List<KitchenJob> getKitchenJobs() {
        if (kitchenJobs == null) {
            kitchenJobs = new ArrayList<>();
            this.kitchenJobs.addAll(CateringAppManager.dataManager.loadKitchenJobs());
        }

        // Restituisce una copia della propria lista per impedire ad altri oggetti di modificarne
        // il contenuto
        List<KitchenJob> ret = new ArrayList<>();
        ret.addAll(kitchenJobs);
        return ret;
    }
}

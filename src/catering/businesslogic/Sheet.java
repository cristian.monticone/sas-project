package catering.businesslogic;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Sheet {
    private Event event;
    private List<Activity> activities = new ArrayList<Activity>();

    public Sheet(Event event){
        this.event = event;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public Activity addActivity(KitchenJob kitchenJob){
        Activity activity = new Activity(kitchenJob);
        activities.add(activity);
        return activity;
    }

    public boolean removeActivity(Activity activity){
        return activities.remove(activity);
    }

    public boolean removeTask(Activity activity,Task task){
        return activity.removeTask(task);
    }

    public Task addTask(Activity activity, Shift shift, Duration time, 
                        Float quantity, User cook, boolean toBePrepared,
                        boolean completed){
        return activity.addTask(shift, time, quantity, cook, toBePrepared, 
                                completed);
    }

    public boolean hasActivity(Activity activity){
        return activities.contains(activity);
    }

    public String toString() {
      return event.toString();
    }
}

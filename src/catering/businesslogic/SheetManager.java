package catering.businesslogic;

import java.util.ArrayList;
import java.util.List;

import java.time.LocalDate;

import java.time.Duration;

import java.lang.IndexOutOfBoundsException;

public class SheetManager {
    private List<SheetEventReceiver> receivers;
    private Sheet currentSheet;
    private List<Sheet> allSheets;


    public SheetManager() {
        receivers = new ArrayList<>();

        receivers.add(new BaseSheetEventReceiver() {
            @Override
            public void notifySheetCreated(Sheet s) {
                allSheets.add(s);
            }
        });
    }

    public void initialize() {};

    public List<Sheet> getAllSheets() {
        if (allSheets == null) {
            allSheets = new ArrayList<>();
            allSheets.addAll(CateringAppManager.dataManager.loadSheets());
        }

        // Restituisce una copia della propria lista per impedire ad altri oggetti di modificarne
        // il contenuto
        List<Sheet> ret = new ArrayList<>();
        ret.addAll(allSheets);
        return ret;
    }

    public Sheet getCurrentSheet() { return currentSheet; }

    public void addReceiver(SheetEventReceiver rec) {
        this.receivers.add(rec);
    }

    public void removeReceiver(SheetEventReceiver rec) {
        this.receivers.remove(rec);
    }

    public Sheet createSheet(Event event) {
        User u = CateringAppManager.userManager.getCurrentUser();
        if (!u.isChef()) 
          throw new UseCaseLogicException("Solo gli chef " +
                                   "possono creare un foglio riepilogativo");
        if (event.getAssignedChef() != u)
          throw new UseCaseLogicException("L'utente " + u + " non è " +
                                        "assegnato all'evento selezionato");

        currentSheet = new Sheet(event);
        for (SheetEventReceiver r: receivers) {
            r.notifySheetCreated(currentSheet);
        }
        return currentSheet;
    }

    public Sheet selectSheet(Sheet sheet) {
        User u = CateringAppManager.userManager.getCurrentUser();
        if (!u.isChef())
            throw new UseCaseLogicException("Solo gli chef " +
                    "possono creare un foglio riepilogativo");
        if (sheet.getEvent() == null)
            throw new UseCaseLogicException("Selezionato un foglio privo di " +
                    "evento ad esso associato.");
        if (sheet.getEvent().getAssignedChef() != u)
            throw new UseCaseLogicException("L'utente " + u + " non è " +
                    "assegnato all'evento selezionato");

        currentSheet = sheet;

        return currentSheet;
    }

    public Activity addActivity(KitchenJob kitchenJob) {
        if (currentSheet == null)
          throw new UseCaseLogicException("Impossibile aggiungere " +
                          "un'attività senza selezionare un foglio");

        Activity activity = currentSheet.addActivity(kitchenJob);

        for (SheetEventReceiver r: receivers) {
            r.notifyActivityAdded(currentSheet, activity);
        }
        return activity;
    }

    public boolean removeActivity(Activity target) {
        if (currentSheet == null)
          throw new UseCaseLogicException("Impossibile rimuovere " +
                          "un'attività senza selezionare un foglio");

        int pos = currentSheet.getActivities().indexOf(target);
        boolean result = currentSheet.removeActivity(target);

        if (result) {
          for (SheetEventReceiver r: receivers) {
              r.notifyActivityRemoved(currentSheet, target,pos);
          }
        }

        return result;
    }

    public boolean removeTask(Activity activity,Task target) {
        if(currentSheet == null || !currentSheet.hasActivity(activity))
            throw new UseCaseLogicException("Impossibile rimuovere " +
                    "un task senza selezionare un foglio");
        boolean result = currentSheet.removeTask(activity,target);

        if(result){
            for (SheetEventReceiver r: receivers) {
                r.notifyTaskRemoved(currentSheet, target);
            }
        }
        return result;
    }

    public Task assignTask(Activity activity, Shift shift, Duration time, Float quantity,
                           User cook, Boolean toBePrepared, Boolean completed) {
        if(currentSheet == null || !currentSheet.hasActivity(activity))
            throw new UseCaseLogicException("Impossibile assegnare " +
                    "un task senza selezionare un foglio");

        Task task = currentSheet.addTask(activity,shift,time,quantity,cook,toBePrepared,completed);

        for (SheetEventReceiver r: receivers) {
            r.notifyAssignTask(currentSheet,activity,task);
        }

        return task;
    }

    public void orderActivity(Activity activity, int position) {
        if (currentSheet == null)
          throw new UseCaseLogicException("Impossibile ordinare " +
                          "un'attività senza selezionare un foglio");
        if (!currentSheet.hasActivity(activity))
          throw new UseCaseLogicException("Impossibile ordinare " +
                          "un'attività non appartenente ad un foglio");

        List<Activity> activities = currentSheet.getActivities();

        if (position < 0 || position >= activities.size())
          throw new IndexOutOfBoundsException("Posizione d'ordinamento non " +
                          "compresa tra il numero d'attività");

        int oldPosition = activities.indexOf(activity);

        activities.remove(activity);

        activities.add(position, activity);

        for (SheetEventReceiver r: receivers) {
            r.notifyActivityRearrangeInSheet(currentSheet, oldPosition, position);
        }
    }

    public void modifyTask(Activity activity, Task task, Shift shift, Duration time,
                           Float quantity, User cook, Boolean toBePrepared,
                           Boolean completed) {
        if (currentSheet == null)
          throw new UseCaseLogicException("Impossibile modificare un task " +
                      "selezionare un foglio");
        if (!currentSheet.hasActivity(activity))
          throw new UseCaseLogicException("L'attività non appartiene al " +
                      "foglio selezionato");
        if (!activity.hasTask(task))
          throw new UseCaseLogicException("Il task selezionato non appartine " +
                      "all'attività selezionata");

        if (shift != null)
          task.setShift(shift);

        if (time != null)
          task.setTime(time);

        if (quantity != null)
          task.setQuantity(quantity);

        if (cook != null)
          task.setCook(cook);

        if (toBePrepared != null)
          task.setToBePrepared(toBePrepared);

        if (completed != null)
          task.setCompleted(completed);

        for (SheetEventReceiver r: receivers) {
            r.notifyModifiedTaskInSheet(currentSheet, activity, task);
        }
    }
}

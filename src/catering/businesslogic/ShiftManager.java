package catering.businesslogic;

import java.util.ArrayList;
import java.util.List;
import catering.persistence.DataManager;

public class ShiftManager {

  private List<Shift> shifts;

  public ShiftManager() {}

  public void initialize() {}

  public List<Shift> getShiftTable() {
    if (shifts == null) {
      shifts = new ArrayList<>();
      try{
        shifts.addAll(CateringAppManager.dataManager.loadShifts());
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }
    List<Shift> ret = new ArrayList<>();
    ret.addAll(shifts);
    return ret;
  }
}

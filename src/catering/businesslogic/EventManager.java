package catering.businesslogic;

import java.util.ArrayList;
import java.util.List;
import catering.persistence.DataManager;

public class EventManager {

  private List<Event> events;

  public EventManager() {}

  public void initialize() {}

  public List<Event> getAllEvents() {
    if (events == null) {
      events = new ArrayList<>();
      try{
        events.addAll(CateringAppManager.dataManager.loadEvents());
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    List<Event> ret = new ArrayList<>();
    ret.addAll(events);
    return ret;
  }
}

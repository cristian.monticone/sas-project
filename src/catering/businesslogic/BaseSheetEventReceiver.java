package catering.businesslogic;

public class BaseSheetEventReceiver implements SheetEventReceiver {
    @Override
    public void notifyActivityRemoved(Sheet s, Activity a, int pos) {

    }

    @Override
    public void notifyActivityRearrangeInSheet(Sheet s, int old_p, int new_p) {

    }

    @Override
    public void notifyModifiedTaskInSheet(Sheet s, Activity a, Task t) {

    }

    @Override
    public void notifyAssignTask(Sheet s, Activity a, Task t) {

    }

    @Override
    public void notifySheetCreated(Sheet s) {

    }

    @Override
    public void notifyActivityAdded(Sheet s, Activity a) {

    }

    @Override
    public void notifyTaskRemoved(Sheet s, Task t) {

    }
}

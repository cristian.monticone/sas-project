package catering.businesslogic;

public class MenuItem implements Cloneable {
    private KitchenJob kitchenJob;
    private String description;

    public MenuItem(KitchenJob kjob, String desc) {
        this.kitchenJob = kjob;
        this.description = desc;
    }

    public MenuItem(KitchenJob kjob) {
        this(kjob, null);
        this.description = kjob.getName();
    }

    public String toString() {
        return this.description;
    }

    public MenuItem clone() {
        MenuItem copia = new MenuItem(this.kitchenJob, this.description);
        return copia;
    }

    public KitchenJob getKitchenJob() {
        return kitchenJob;
    }

    public void setKitchenJob(KitchenJob kitchenJob) {
        this.kitchenJob = kitchenJob;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

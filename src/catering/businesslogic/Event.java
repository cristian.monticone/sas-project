package catering.businesslogic;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;

public class Event {
  private String name;
  private String location;
  private LocalDate date;
  private int numberOfParticipants;
  private String serviceType;
  private String tipologyNote;
  private String annotation;
  private boolean closed;
  private User organiser;
  private User assignedChef;
  private Menu menu;
  //TODO:private List<Role> roles;

  //TODO:Create roles and values to attributes
  public Event(User organiser) {
    setOrganiser(organiser);
  }

  public Event(String name, String location, LocalDate date,
      int numberOfParticipants, String serviceType, String tipologyNote,
      String annotation, boolean closed, User organiser, User assignedChef,
      Menu menu) {
    this.name = name;
    this.location = location;
    this.date = date;
    this.numberOfParticipants = numberOfParticipants;
    this.serviceType = serviceType;
    this.tipologyNote = tipologyNote;
    this.annotation = annotation;
    this.closed = closed;
    this.organiser = organiser;
    this.assignedChef = assignedChef;
    this.menu = menu;
  }

  //SETTER
  public void setName(String name){
    this.name = name;
  }

  public void setLocation(String location){
    this.location = location;
  }

  public void setDate(LocalDate date){
    this.date = date;
  }

  public void setNumberOfParticipants(int number){
    numberOfParticipants = number;
  }

  public void setServiceType(String serviceType){
    this.serviceType = serviceType;
  }

  public void setTipologyNote(String tipologyNote){
    this.tipologyNote = tipologyNote;
  }

  public void setAnnotation(String annotation){
    this.annotation = annotation;
  }

  public void setClosed(boolean closed){
    this.closed = closed;
  }

  public void setOrganiser(User organiser) {
    this.organiser = organiser;
  }

  public void setAssignedChef(User chef) {
    assignedChef = chef;
  }

  public void setMenu(Menu menu){
    this.menu = menu;
  }

  //GETTER
  public String getName() {
    return name;
  }
  public User getAssignedChef() {
    return assignedChef;
  }

  public User getOrganiser() {
    return organiser;
  }
  
  public String getLocation(){
    return location;
  }
  public LocalDate getDate(){
    return date;
  }
  public int getNumberOfParticipants(){
    return numberOfParticipants;
  }
  public String getServiceType(){
    return serviceType;
  }
  public String getTipologyNote(){
    return tipologyNote;
  }
  public String getAnnotation(){
    return annotation;
  }
  public boolean getClosed(){
    return closed;
  }

  public Menu getMenu(){
    return menu;
  }

  public String toString() {
    return name + ", Chef assegnato: " + assignedChef;
  }
}

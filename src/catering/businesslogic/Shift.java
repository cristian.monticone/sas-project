package catering.businesslogic;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Duration;

public class Shift {
  private LocalDate day;
  private LocalTime startTime;
  private LocalTime endTime;
  private boolean inPlace;
  private boolean complete;

  public Shift(LocalDate day, LocalTime startTime, LocalTime endTime, boolean inPlace,
               boolean complete) {
    this.day = day; 
    this.startTime = startTime; 
    this.endTime = endTime; 
    this.inPlace = inPlace; 
    this.complete = complete; 
  }

  public String toString() {
    
    String ret = "";
    if(day != null)
      ret = ret +"Giorno: "+day;
    if(startTime != null)
      ret = ret +", Alle: "+startTime;
    return ret;
  }
}

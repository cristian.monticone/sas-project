package catering.businesslogic;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Activity {

    private KitchenJob kitchenJob;
    private List<Task> tasks;

    public Activity(KitchenJob kitchenJob){
        this.kitchenJob = kitchenJob;
        this.tasks = new ArrayList<Task>();
    }

    public KitchenJob getKitchenJob() {
        return kitchenJob;
    }

    public void setKitchenJob(KitchenJob kitchenJob) {
        this.kitchenJob = kitchenJob;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public boolean hasTask(Task task){
        return tasks.contains(task);
    }

    public boolean removeTask(Task task){
        return tasks.remove(task);
    }

    public Task addTask(Shift shift, Duration time, Float quantity, User cook, 
                        boolean toBePrepared, boolean completed){
        Task task = new Task(shift, time, quantity, cook, toBePrepared,
                             completed);
        tasks.add(task);
        return task;
    }

    @Override
    public String toString() {
        return kitchenJob.toString();
    }
}

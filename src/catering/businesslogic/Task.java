package catering.businesslogic;

import java.time.Duration;

public class Task {
    private Shift shift;
    private Duration time;
    private Float quantity;
    private User cook;
    private boolean toBePrepared;
    private boolean completed;

    public Task(Shift shift, Duration time,Float quantity, User cook,
                Boolean toBePrepared, Boolean completed){
        this.shift = shift;
        if(time == null)
            this.time = Duration.ZERO;
        else
            this.time = time;
        if(quantity == null)
            this.quantity = Float.valueOf(0);
        else
            this.quantity = quantity;
        if(cook != null)
            this.cook = cook;
        if(toBePrepared == null)
            this.toBePrepared = false;
        else
            this.toBePrepared = toBePrepared;
        if(completed == null)
            this.completed = false;
        else
            this.completed = completed;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public Duration getTime() {
        return time;
    }

    public void setTime(Duration time) {
        this.time = time;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public User getCook() {
        return cook;
    }

    public void setCook(User cook) {
        this.cook = cook;
    }

    public boolean isToBePrepared() {
        return toBePrepared;
    }

    public void setToBePrepared(boolean toBePrepared) {
        this.toBePrepared = toBePrepared;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String toString() {
      String s = "";

      if (cook!=null)
        s += cook + ", ";

      if (quantity!= null)
        s += "Quantità: " + quantity + ", ";

      if (time!=null)
        s += "time: " + time.toMinutes() + " minuti";

      return s + ", " +  shift;
    }
}

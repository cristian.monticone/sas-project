package catering.businesslogic;

public interface SheetEventReceiver {
    public void notifyActivityRemoved(Sheet s, Activity a, int pos);
    public void notifyActivityRearrangeInSheet(Sheet s, int old_p, int new_p);
    public void notifyModifiedTaskInSheet(Sheet s, Activity a, Task t);
    public void notifyAssignTask(Sheet s, Activity a, Task t);
    public void notifySheetCreated(Sheet s);
    public void notifyActivityAdded(Sheet s, Activity a);
    public void notifyTaskRemoved(Sheet s, Task t);
}

package catering.persistence;

import catering.businesslogic.*;

import java.sql.*;
import java.util.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Duration;

public class DataManager {
  private String userName = "root";
  private String password = "";
  private String serverName = "localhost";
  private String portNumber = "3306";

  private Connection connection;

  //Il DataManager deve tener traccia di quali oggetti in memoria
  //corrispondono a quali record del DB. Per questo usa una doppia
  //mappa per ciascun tipo di oggetto caricato
  private Map<User, Integer> userObjects;
  private Map<Integer, User> idToUserObject;

  private Map<KitchenJob, Integer> kitchenJobObjects;
  private Map<Integer, KitchenJob> idToKitchenJobObject;

  private Map<Menu, Integer> menuObjects;
  private Map<Integer, Menu> idToMenuObject;

  private Map<Section, Integer> sectionObjects;
  private Map<Integer, Section> idToSectionObject;

  private Map<MenuItem, Integer> itemObjects;
  private Map<Integer, MenuItem> idToItemObject;

  private Map<Shift, Integer> shiftObjects;
  private Map<Integer, Shift> idToShiftObject;

  private Map<Event, Integer> eventObjects;
  private Map<Integer, Event> idToEventObject;

  private Map<Sheet, Integer> sheetObjects;
  private Map<Integer, Sheet> idToSheetObject;

  private Map<Activity, Integer> activityObjects;
  private Map<Integer, Activity> idToActivityObject;

  private Map<Task, Integer> taskObjects;
  private Map<Integer, Task> idToTaskObject;    

  public DataManager() {

    this.userObjects = new HashMap<>();
    this.idToUserObject = new HashMap<>();
    this.kitchenJobObjects = new HashMap<>();
    this.idToKitchenJobObject = new HashMap<>();
    this.menuObjects = new HashMap<>();
    this.idToMenuObject = new HashMap<>();
    this.sectionObjects = new HashMap<>();
    this.idToSectionObject = new HashMap<>();
    this.itemObjects = new HashMap<>();
    this.idToItemObject = new HashMap<>();
    this.shiftObjects = new HashMap<>();
    this.idToShiftObject = new HashMap<>();
    this.taskObjects = new HashMap<>();
    this.idToTaskObject = new HashMap<>();
    this.eventObjects = new HashMap<>();
    this.idToEventObject = new HashMap<>();
    this.sheetObjects = new HashMap<>();
    this.idToSheetObject = new HashMap<>();
    this.activityObjects = new HashMap<>();
    this.idToActivityObject = new HashMap<>();

  }

  public void initialize() throws SQLException {
    Connection conn = null;
    Properties connectionProps = new Properties();
    connectionProps.put("user", this.userName);
    connectionProps.put("password", this.password);
    connectionProps.put("useUnicode", true);
    connectionProps.put("useJDBCCompliantTimezoneShift", true);
    connectionProps.put("useLegacyDatetimeCode", false);
    connectionProps.put("serverTimezone", "UTC");

    conn = DriverManager.getConnection("jdbc:mysql://" +this.serverName +
        ":" + this.portNumber + "/catering",connectionProps);
    System.out.println("Connected to database");
    this.connection = conn;


    CateringAppManager.menuManager.addReceiver(new MenuEventReceiver() {
      @Override
      public void notifyMenuCreated(Menu m) {
        int mid = writeNewMenu(m);
        menuObjects.put(m, mid);
        idToMenuObject.put(mid, m);
        List<Section> secs = m.getSections();
        for (int i = 0; i < secs.size(); i++) {
          Section s = secs.get(i);
          int sid = writeNewSection(mid, i, s);
          sectionObjects.put(s, sid);
          idToSectionObject.put(sid, s);

          List<MenuItem> secItems = s.getItems();

          for (int j = 0; j < secItems.size(); j++) {
            MenuItem it = secItems.get(j);
            int iid = writeNewItem(mid, sid, j, it);
            itemObjects.put(it, iid);
            idToItemObject.put(iid, it);
          }
        }

        List<MenuItem> menuItems = m.getItemsWithoutSection();
        for (int z=0; z < menuItems.size(); z++) {
          MenuItem it = menuItems.get(z);
          int iid = writeNewItem(mid, 0, z, it); 
          itemObjects.put(it, iid);
          idToItemObject.put(iid, it);
        }
      }

      @Override
      public void notifySectionAdded(Menu m, Section s) {
        int mid = menuObjects.get(m);
        int pos = m.getSectionPosition(s);
        int sid = writeNewSection(mid, pos, s);
        sectionObjects.put(s, sid);
        idToSectionObject.put(sid, s);
      }

      @Override
      public void notifyItemAdded(Menu m, Section s, MenuItem it) {
        int mid = menuObjects.get(m);
        int sid, pos;
        if (s != null) {
          sid = sectionObjects.get(s);
          pos = s.getItemPosition(it);
        } else {
          sid = 0;
          pos = m.getItemPosition(it);
        }
        int iid = writeNewItem(mid, sid, pos, it);
        itemObjects.put(it, iid);
        idToItemObject.put(iid, it);
      }

      @Override
      public void notifyMenuPublished(Menu m) {
        writeMenuChanges(m);

      }

      @Override
      public void notifyMenuDeleted(Menu m) {
        removeMenu(m);
      }

      @Override
      public void notifySectionRemoved(Menu m, Section s) {
        removeSection(s);
      }


      @Override
      public void notifySectionNameChanged(Menu m, Section s) {
        int mid = menuObjects.get(m);
        int pos = m.getSectionPosition(s);
        writeSectionChanges(mid, pos, s);
      }

      @Override
      public void notifySectionsRearranged(Menu m) {
        List<Section> sects = m.getSections();
        int mid = menuObjects.get(m);
        for (int i = 0; i < sects.size(); i++) {
          writeSectionChanges(mid, i, sects.get(i));
        }

      }

      @Override
      public void notifyItemsRearranged(Menu m, Section s) {
        List<MenuItem> its = s.getItems();
        int mid = menuObjects.get(m);
        int sid = sectionObjects.get(s);
        for (int i = 0; i < its.size(); i++) {
          writeItemChanges(mid, sid, i, its.get(i));
        }
      }

      @Override
      public void notifyItemsRearrangedInMenu(Menu m) {
        List<MenuItem> its = m.getItemsWithoutSection();
        int mid = menuObjects.get(m);
        for (int i = 0; i < its.size(); i++) {
          writeItemChanges(mid, 0, i, its.get(i));
        }

      }

      @Override
      public void notifyItemMoved(Menu m, Section oldS, Section newS, MenuItem it) {
        int mid = menuObjects.get(m);
        int sid = (newS == null ? 0 : sectionObjects.get(newS));
        int itpos = (newS == null ? m.getItemPosition(it) : newS.getItemPosition(it));
        writeItemChanges(mid, sid, itpos, it);
      }

      @Override
      public void notifyItemDescriptionChanged(Menu m, MenuItem it) {
        int mid = menuObjects.get(m);
        Section s = m.getSection(it);
        int sid = (s == null ? 0 : sectionObjects.get(s));
        int itpos = (s == null ? m.getItemPosition(it) : s.getItemPosition(it));
        writeItemChanges(mid, sid, itpos, it);
      }

      @Override
      public void notifyItemDeleted(Menu m, MenuItem it) {
        removeItem(it);
      }

      @Override
      public void notifyMenuTitleChanged(Menu m) {
        writeMenuChanges(m);
      }
    });
    CateringAppManager.sheetManager.addReceiver( new SheetEventReceiver() {
      @Override
      public void notifySheetCreated(Sheet s){
        int sid = writeNewSheet(s);
        sheetObjects.put(s,sid);
        idToSheetObject.put(sid,s);
        List<Activity> activities = s.getActivities();
        for(Activity act: activities) {
          int aid = writeNewActivity(s,act);
          activityObjects.put(act,aid);
          idToActivityObject.put(aid,act);
          List<Task> tasks = act.getTasks();
          for(Task ts: tasks) {
            int tid = writeNewTask(act,ts);
            taskObjects.put(ts,tid);
            idToTaskObject.put(tid,ts);
          }
        }
      }
      @Override
      public void notifyActivityAdded(Sheet s, Activity act){
        int aid = writeNewActivity(s,act);
        activityObjects.put(act,aid);
        idToActivityObject.put(aid,act);
        List<Task> tasks = act.getTasks();
        for(Task ts: tasks) {
          int tid = writeNewTask(act,ts);
          taskObjects.put(ts,tid);
          idToTaskObject.put(tid,ts);
        }
      }
      @Override
      public void notifyActivityRemoved(Sheet s, Activity a, int pos){
        removeActivity(a,pos);
      }
      @Override
      public void notifyActivityRearrangeInSheet(Sheet s, int old_p,
          int new_p){
        orderActivity(s,old_p,new_p);
      }
      @Override
      public void notifyModifiedTaskInSheet(Sheet s, Activity a, Task t){
        modifyTask(t);
      }
      @Override
      public void notifyAssignTask(Sheet s, Activity act, Task ts) {
        int tid = writeNewTask(act,ts);
        taskObjects.put(ts,tid);
        idToTaskObject.put(tid,ts);
      }
      @Override
      public void notifyTaskRemoved(Sheet s, Task t){
        removeTask(t);
      }
    });
  }

  private int writeNewMenu(Menu m) {
    String sql = "INSERT INTO Menus(title, menuowner, published, fingerFood, " +
      "cookRequired, hotDishes, kitchenRequired, buffet) "
      + "VALUES(?,?,?,?,?,?,?,?)";
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setString(1, m.getTitle());
      pstmt.setInt(2, this.userObjects.get(m.getOwner()));
      pstmt.setBoolean(3, m.isPublished());
      pstmt.setBoolean(4, m.isFingerFood());
      pstmt.setBoolean(5, m.isCookRequired());
      pstmt.setBoolean(6, m.isHotDishes());
      pstmt.setBoolean(7, m.isKitchenRequired());
      pstmt.setBoolean(8, m.isBuffet());

      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return id;
  }

  private void writeMenuChanges(Menu m) {
    int mid = menuObjects.get(m);
    int uid = userObjects.get(m.getOwner());
    String sql = "UPDATE Menus SET menuowner=?, published=?, fingerFood=?, "
      +"cookRequired=?, hotDishes=?, kitchenRequired=?, buffet=?, title=? "
      +"WHERE id=" + mid;
    PreparedStatement pstmt = null;

    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, uid);
      pstmt.setBoolean(2, m.isPublished());
      pstmt.setBoolean(3, m.isFingerFood());
      pstmt.setBoolean(4, m.isCookRequired());
      pstmt.setBoolean(5, m.isHotDishes());
      pstmt.setBoolean(6, m.isKitchenRequired());
      pstmt.setBoolean(7, m.isBuffet());
      pstmt.setString(8, m.getTitle());

      int r = pstmt.executeUpdate();
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private void removeMenu(Menu m) {
    int mId = menuObjects.get(m);
    String sqlItems = "DELETE FROM MenuItems WHERE menu=?";
    String sqlSections = "DELETE FROM Sections WHERE menu=?";
    String sqlMenu = "DELETE FROM Menus WHERE id=?";
    PreparedStatement pstItems = null;
    PreparedStatement pstSections = null;
    PreparedStatement pstMenu = null;
    try {
      connection.setAutoCommit(false);
      pstItems = connection.prepareStatement(sqlItems);
      pstSections = connection.prepareStatement(sqlSections);
      pstMenu = connection.prepareStatement(sqlMenu);
      pstItems.setInt(1, mId);
      pstSections.setInt(1, mId);
      pstMenu.setInt(1, mId);
      pstItems.executeUpdate();
      pstSections.executeUpdate();
      pstMenu.executeUpdate();
      connection.commit();
    } catch (SQLException exc) {
      exc.printStackTrace();
      try {
        connection.rollback();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    } finally {
      try {
        connection.setAutoCommit(true);
        if (pstItems != null) pstItems.close();
        if (pstSections != null) pstSections.close();
        if (pstMenu != null) pstMenu.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }


  private int writeNewSection(int menuId, int position, Section sec) {

    String sql = "INSERT INTO Sections(menu, name, position) VALUES(?,?,?)";
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, menuId);
      pstmt.setString(2, sec.getName());
      pstmt.setInt(3, position);

      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }

    return id;
  }

  private void writeSectionChanges(int menuId, int position, Section sec) {
    int sId = sectionObjects.get(sec);
    String sql = "UPDATE Sections SET menu=?, name=?, position=? WHERE id=" + sId;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, menuId);
      pstmt.setString(2, sec.getName());
      pstmt.setInt(3, position);

      pstmt.executeUpdate();

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private void removeSection(Section s) {
    int sId = sectionObjects.get(s);
    String sqlItems = "DELETE FROM MenuItems WHERE section=?";
    String sqlSection = "DELETE FROM Sections WHERE id=?";
    PreparedStatement pstItems = null;
    PreparedStatement pstSection = null;
    try {
      connection.setAutoCommit(false);
      pstItems = connection.prepareStatement(sqlItems);
      pstSection = connection.prepareStatement(sqlSection);
      pstItems.setInt(1, sId);
      pstSection.setInt(1, sId);
      pstItems.executeUpdate();
      pstSection.executeUpdate();
      connection.commit();
    } catch (SQLException exc) {
      exc.printStackTrace();
      try {
        connection.rollback();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    } finally {
      try {
        connection.setAutoCommit(true);
        if (pstItems != null) pstItems.close();
        if (pstSection != null) pstSection.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private int writeNewItem(int menuId, int secId, int position, MenuItem item) {

    String sql = "INSERT INTO MenuItems(menu, section, description, kitchenjob, position) " +
      "VALUES(?,?,?,?,?)";
    int id = -1;
    PreparedStatement pstmt = null;

    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, menuId);
      pstmt.setInt(2, secId);
      pstmt.setString(3, item.getDescription());
      pstmt.setInt(4, kitchenJobObjects.get(item.getKitchenJob()));
      pstmt.setInt(5, position);

      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }

    return id;
  }

  private void writeItemChanges(int menuId, int secId, int position, MenuItem item) {
    int itId = itemObjects.get(item);
    String sql = "UPDATE MenuItems SET menu=?, section=?, description=?, kitchenjob=?, position=? WHERE " +
      "id=" + itId;
    PreparedStatement pstmt = null;

    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, menuId);
      pstmt.setInt(2, secId);
      pstmt.setString(3, item.getDescription());
      pstmt.setInt(4, kitchenJobObjects.get(item.getKitchenJob()));
      pstmt.setInt(5, position);

      pstmt.executeUpdate();

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private void removeItem(MenuItem it) {
    int iId = itemObjects.get(it);
    String sqlItems = "DELETE FROM MenuItems WHERE id=?";
    PreparedStatement pstItems = null;
    try {
      pstItems = connection.prepareStatement(sqlItems);
      pstItems.setInt(1, iId);
      pstItems.executeUpdate();
    } catch (SQLException exc) {
      exc.printStackTrace();

    } finally {
      try {
        if (pstItems != null) pstItems.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }

    }
  }



  public int writeNewEvent(Event ev) {
    String sql = "INSERT INTO Events(menu, organiser, assignedChef, name, "
      +"location, date, numberOfParticipants, serviceType, tipologyNote, "
      +"annotation, closed) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, this.menuObjects.get(ev.getMenu()));
      pstmt.setInt(2, this.userObjects.get(ev.getOrganiser()));
      pstmt.setInt(3, this.userObjects.get(ev.getAssignedChef()));
      pstmt.setString(4, ev.getName());
      pstmt.setString(5, ev.getLocation());
      pstmt.setString(6, ev.getDate().toString());
      pstmt.setInt(7, ev.getNumberOfParticipants());
      pstmt.setString(8, ev.getServiceType());
      pstmt.setString(9, ev.getTipologyNote());
      pstmt.setString(10, ev.getAnnotation());
      pstmt.setBoolean(11, ev.getClosed());

      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return id;
  }

  public void writeEventChanges(Event ev) {
    int eid = eventObjects.get(ev);
    String sql = "UPDATE Events SET menu = ?, organiser = ?, assignedChef = ?, "
      +"name = ?, numberOfParticipants = ?, serviceType = ?, tipologyNote = ?, "
      +"annotation = ?, closed = ? WHERE id=" + eid;
    PreparedStatement pstmt = null;

    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);

      pstmt.setInt(1, this.menuObjects.get(ev.getMenu()));
      pstmt.setInt(2, this.userObjects.get(ev.getOrganiser()));
      pstmt.setInt(3, this.userObjects.get(ev.getAssignedChef()));
      pstmt.setString(4, ev.getName());
      pstmt.setString(5, ev.getLocation());
      pstmt.setString(6, ev.getDate().toString());
      pstmt.setInt(7, ev.getNumberOfParticipants());
      pstmt.setString(8, ev.getServiceType());
      pstmt.setString(9, ev.getTipologyNote());
      pstmt.setString(10, ev.getAnnotation());
      pstmt.setBoolean(11, ev.getClosed());

      int r = pstmt.executeUpdate();
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private int writeNewSheet(Sheet s){
    String sql = "INSERT INTO Sheets(event) VALUES(?)";
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, this.eventObjects.get(s.getEvent()));
      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return id;
  }

  private void removeSheet(Sheet s) {
  }

  private int writeNewActivity(Sheet s, Activity act){
    String sql = "INSERT INTO Activities(kitchenjob, sheet, position)"
     +" VALUES(?,?,?)";
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, kitchenJobObjects.get(act.getKitchenJob()));
      pstmt.setInt(2, sheetObjects.get(s));
      pstmt.setInt(3, s.getActivities().indexOf(act));
      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return id;
  }

  private int writeNewTask(Activity act, Task ts){
    String sql = "INSERT INTO Tasks(activity, shift, cook, time, quantity, "
      +"toBePrepared, completed) VALUES(?,?,?,?,?,?,?)";
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, activityObjects.get(act));
      pstmt.setInt(2, shiftObjects.get(ts.getShift()));
      pstmt.setInt(3, userObjects.get(ts.getCook()));
      pstmt.setString(4, ts.getTime().toString());
      pstmt.setDouble(5, ts.getQuantity());
      pstmt.setBoolean(6, ts.isToBePrepared());
      pstmt.setBoolean(7, ts.isCompleted());
      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return id;
  }

  private void orderActivity(Sheet s, int oldPos, int newPos) {
    Activity act = s.getActivities().get(newPos);
    int diff = oldPos - newPos;
    int aid = this.activityObjects.get(act);
    int sid = this.sheetObjects.get(s);
    String queryShift, assignNewPos;
    PreparedStatement pstShift = null;
    PreparedStatement pstNewPos = null;
    assignNewPos = "UPDATE Activities SET position = "+newPos
      +" WHERE id = "+aid+" AND sheet = "+sid;
    if(diff == 0)
      return;
    if(diff < 0){
      queryShift = "UPDATE Activities SET position = position - 1 "
        +"WHERE position <= "+newPos+" AND position > "+oldPos+" AND sheet = "+sid;
    }
    else {
      queryShift = "UPDATE Activities SET position = position + 1 "
        +"WHERE position >= "+newPos+" AND position < "+oldPos+" AND sheet = "+sid;
    }
    try {
      connection.setAutoCommit(false);
      pstShift = connection.prepareStatement(queryShift);
      pstNewPos = connection.prepareStatement(assignNewPos);
      pstShift.executeUpdate();
      pstNewPos.executeUpdate();
      connection.commit();
    } catch (SQLException exc) {
      exc.printStackTrace();
      try {
        connection.rollback();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    } finally {
      try {
        connection.setAutoCommit(true);
        if (pstShift != null) pstShift.close();
        if (pstNewPos != null) pstNewPos.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private void removeActivity(Activity act, int position){
    int aId = activityObjects.get(act);
    String sqlTask = "DELETE FROM Tasks WHERE activity="+aId;
    String sqlActivity = "DELETE FROM Activities WHERE id="+aId;
    String sqlUpdatePosition = "UPDATE Activities SET position = position - 1 "
      +"WHERE position > "+position;
    PreparedStatement pstTask = null;
    PreparedStatement pstActivity = null;
    PreparedStatement pstUpdatePosition= null;
    try {
      connection.setAutoCommit(false);
      pstTask = connection.prepareStatement(sqlTask);
      pstActivity = connection.prepareStatement(sqlActivity);
      pstUpdatePosition = connection.prepareStatement(sqlUpdatePosition);
      pstUpdatePosition.executeUpdate();
      pstTask.executeUpdate();
      pstActivity.executeUpdate();
      connection.commit();
    } catch (SQLException exc) {
      exc.printStackTrace();
      try {
        connection.rollback();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    } finally {
      try {
        connection.setAutoCommit(true);
        if (pstTask != null) pstTask.close();
        if (pstActivity != null) pstActivity.close();
        if (pstUpdatePosition != null) pstUpdatePosition.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private int modifyTask(Task ts){
    int tId = taskObjects.get(ts);
    String sql = "UPDATE Tasks SET shift = ?, cook = ?, time = ?, "
      +"quantity = ?, toBePrepared = ?, completed = ? WHERE id ="+tId;
    int id = -1;
    PreparedStatement pstmt = null;
    try {
      pstmt = this.connection.prepareStatement(sql,
          Statement.RETURN_GENERATED_KEYS);
      pstmt.setInt(1, shiftObjects.get(ts.getShift()));
      pstmt.setInt(2, userObjects.get(ts.getCook()));
      pstmt.setString(3, ts.getTime().toString());
      pstmt.setDouble(4, ts.getQuantity());
      pstmt.setBoolean(5, ts.isToBePrepared());
      pstmt.setBoolean(6, ts.isCompleted());
      int r = pstmt.executeUpdate();

      if (r == 1) {
        ResultSet rs = pstmt.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstmt != null) pstmt.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return id;
  }

  private void removeTask(Task t) {
    int tId = taskObjects.get(t);
    String sqlItems = "DELETE FROM Tasks WHERE id=?";
    PreparedStatement pstItems = null;
    try {
      pstItems = connection.prepareStatement(sqlItems);
      pstItems.setInt(1, tId);
      pstItems.executeUpdate();
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pstItems != null) pstItems.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }
  
  //Sezione caricamento da DB entità
  public List<Sheet> loadSheets() {
    Statement st = null;
    String query = "SELECT * FROM Sheets";
    List<Sheet> ret = new ArrayList<>();
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        int eid = rs.getInt("event");
        Event ev = innerLoadEvent(rs.getInt("event"));
        this.eventObjects.put(ev, eid);
        this.idToEventObject.put(eid, ev);
        //Verifica se per caso l'ha già caricata
        Sheet sheet = this.idToSheetObject.get(id);

        if (sheet == null) 
          sheet = new Sheet(ev);

        if (sheet != null) {
          ret.add(sheet);
          loadSheetActivities(id,sheet);
          this.sheetObjects.put(sheet, id);
          this.idToSheetObject.put(id, sheet);
        }
      }

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }

  private void loadSheetActivities(int sheetId, Sheet sheet) {
    Statement st = null;
    String query = "SELECT * FROM Activities WHERE sheet =" + sheetId
      + " ORDER BY position";
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        KitchenJob kitchenJob = innerLoadKitchenJob(rs.getInt("kitchenjob"));
        Activity act = sheet.addActivity(kitchenJob);
        loadActivityTasks(id,act);
        this.activityObjects.put(act, id);
        this.idToActivityObject.put(id, act);
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }
  
  private void loadActivityTasks(int actId, Activity act) {
    Statement st = null;
    String query = "SELECT * FROM Tasks WHERE activity =" + actId;
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        Shift shift = this.innerLoadShift(rs.getInt("shift"));
        Duration time = Duration.parse(rs.getString("time"));
        float quantity = rs.getFloat("quantity");
        User cook = innerLoadUser(rs.getInt("cook"));
        boolean toBePrepared = rs.getBoolean("toBePrepared");
        boolean completed = rs.getBoolean("completed");
        Task task = act.addTask(shift, time, quantity, cook, toBePrepared, 
            completed);
        this.taskObjects.put(task, id);
        this.idToTaskObject.put(id, task);
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  public List<Task> loadTasks() {
    Statement st = null;
    String query = "SELECT * FROM Tasks";
    List<Task> ret = new ArrayList<>();

    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        Shift shift = this.innerLoadShift(rs.getInt("shift"));
        Duration time = Duration.parse(rs.getString("time"));
        float quantity = rs.getFloat("quantity");
        User cook = innerLoadUser(rs.getInt("cook"));
        boolean toBePrepared = rs.getBoolean("toBePrepared");
        boolean completed = rs.getBoolean("completed");
        //Verifica se per caso l'ha già caricata
        Task task = this.idToTaskObject.get(id);

        if (task == null) 
          task = new Task(shift, time, quantity, cook, toBePrepared,
              completed);

        if (task != null) {
          ret.add(task);
          this.taskObjects.put(task, id);
          this.idToTaskObject.put(id, task);
        }
      }

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }

  private Shift innerLoadShift(int idShift) {
    //verifico se l'ho già caricato in precedenza
    Shift shift = this.idToShiftObject.get(idShift);
    if (shift != null) return shift;

    Statement st = null;

    String query = "SELECT * FROM Shifts WHERE Shifts.id = " + idShift;
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      if (rs.next()) {
        int id = rs.getInt("id");
        LocalDate day = LocalDate.parse(rs.getString("day"));
        LocalTime startTime = LocalTime.parse(rs.getString("startTime"));
        LocalTime endTime = LocalTime.parse(rs.getString("endTime"));
        boolean inPlace = rs.getBoolean("inPlace"); boolean complete = rs.getBoolean("complete");
        shift = new Shift(day, startTime, endTime, inPlace, complete);

        this.shiftObjects.put(shift, idShift);
        this.idToShiftObject.put(idShift, shift);
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }

    return shift;
  }
  public User loadUser(String userName) {
    PreparedStatement pst = null;
    String sql = "SELECT Users.id, Users.name, UserRoles.role FROM Users LEFT JOIN UserRoles on Users.id = "
      + "UserRoles.user where Users.name=? ";
    User u = null;

    try {
      pst = this.connection.prepareStatement(sql);
      pst.setString(1, userName);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        if (u == null) {
          u = new User(userName);
          int id = rs.getInt("id");
          this.userObjects.put(u, id);
          this.idToUserObject.put(id, u);
        }

        addUserRole(u, rs);

      }
      pst.close();
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pst != null) pst.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return u;
  }

  public List<User> loadUsers() {
    PreparedStatement pst = null;
    String sql = "SELECT Users.id, Users.name, UserRoles.role FROM Users "
      +"LEFT JOIN UserRoles on Users.id = UserRoles.user WHERE UserRoles.role = 'c'";
    List<User> ret = new ArrayList<>();

    try {
      pst = this.connection.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
          int id = rs.getInt("id");
          User u = this.idToUserObject.get(id);
          if(u == null) {
            u = new User(rs.getString("name"));
            addUserRole(u, rs);
          }
          if(u != null){
            ret.add(u);
            this.userObjects.put(u, id);
            this.idToUserObject.put(id, u);
        }
      }
      pst.close();
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (pst != null) pst.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }

  public List<KitchenJob> loadKitchenJobs() {
    Statement st = null;
    String query = "SELECT * FROM KitchenJobs";
    List<KitchenJob> ret = new ArrayList<>();

    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        String name = rs.getString("name");
        char type = rs.getString("type").charAt(0);
        int id = rs.getInt("id");

        //Verifica se per caso l'ha già caricata
        KitchenJob kjob = this.idToKitchenJobObject.get(id);

        if (kjob == null) 
          kjob = createKitchenJobWithType(name, type);

        if (kjob != null) {
          ret.add(kjob);
          this.kitchenJobObjects.put(kjob, id);
          this.idToKitchenJobObject.put(id, kjob);
        }
      }

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }

  public List<Menu> loadMenus() {
    List<Menu> ret = new ArrayList<>();
    Statement st = null;
    String query = "SELECT * FROM Menus";

    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");

        //Verifica se per caso l'ha già caricato
        Menu m = this.idToMenuObject.get(id);
        if (m == null) {

          String title = rs.getString("title");

          int ownerid = rs.getInt("menuowner");
          User owner = this.innerLoadUser(ownerid);

          m = new Menu(owner, title);
          m.setPublished(rs.getBoolean("published"));
          m.setBuffet(rs.getBoolean("buffet"));
          m.setCookRequired(rs.getBoolean("cookRequired"));
          m.setFingerFood(rs.getBoolean("fingerFood"));
          m.setHotDishes(rs.getBoolean("hotDishes"));
          m.setKitchenRequired(rs.getBoolean("kitchenRequired"));

          //per sapere se il menu è in uso consulto la tabella degli eventi
          //NdR: un menu è in uso anche se l'evento che lo usa è concluso o annullato
          Statement st2 = this.connection.createStatement();
          String query2 = "SELECT Events.id FROM Events JOIN Menus M on Events.menu = M.id WHERE M.id=" + id;
          ResultSet rs2 = st2.executeQuery(query2);
          m.setInUse(rs2.next());
          st2.close();
          loadMenuSections(id, m);
          loadMenuItems(id, m);

          ret.add(m);
          this.menuObjects.put(m, id);
          this.idToMenuObject.put(id, m);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }

  public List<Shift> loadShifts() {
    Statement st = null;
    String query = "SELECT * FROM Shifts";
    List<Shift> ret = new ArrayList<>();
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        LocalDate day = LocalDate.parse(rs.getString("day"));
        LocalTime startTime = LocalTime.parse(rs.getString("startTime"));
        LocalTime endTime = LocalTime.parse(rs.getString("endTime")); 
        boolean inPlace = rs.getBoolean("inPlace");
        boolean complete = rs.getBoolean("complete");

        //Verifica se per caso l'ha già caricata
        Shift shift = this.idToShiftObject.get(id);

        if (shift == null) 
          shift = new Shift(day, startTime, endTime, inPlace, complete);

        if (shift != null) {
          ret.add(shift);
          this.shiftObjects.put(shift, id);
          this.idToShiftObject.put(id, shift);
        }
      }

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }

  public List<Event> loadEvents() {
    Statement st = null;
    String query = "SELECT * FROM Events";
    List<Event> ret = new ArrayList<>();
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String location = rs.getString("location");
        LocalDate date = LocalDate.parse(rs.getString("date"));
        int numberOfParticipants = rs.getInt("numberOfParticipants");
        String serviceType = rs.getString("serviceType");
        String tipologyNote = rs.getString("tipologyNote");
        String annotation = rs.getString("annotation");
        boolean closed = rs.getBoolean("closed");
        User organiser = innerLoadUser(rs.getInt("organiser"));
        User assignedChef = innerLoadUser(rs.getInt("assignedChef"));
        Menu menu = innerLoadMenu(rs.getInt("menu"));
        //Verifica se per caso l'ha già caricata
        Event event = this.idToEventObject.get(id);

        if (event == null) {
          event = new Event(name, location, date, numberOfParticipants,
              serviceType, tipologyNote, annotation, closed, organiser,
              assignedChef, menu);
        }

        if (event != null) {
          ret.add(event);
          this.eventObjects.put(event, id);
          this.idToEventObject.put(id, event);
        }
      }

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return ret;
  }


  private void loadMenuItems(int id, Menu m) {
    //Caricamento voci
    //Non verifichiamo se un MenuItem è già stato creato perché
    //questo può avvenire solo nel contesto del caricamento di un Menu
    //e il MenuItem può essere già creato solo se il Menu è stato creato;
    //il controllo sul Menu avviene già in loadMenus
    Statement st = null;
    String query = "SELECT MenuItems.* FROM MenuItems WHERE MenuItems.menu=" + id
      + " ORDER BY MenuItems.position";
    try {
      st = this.connection.createStatement();

      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {
        String description = rs.getString("description");
        int idSec = rs.getInt("section");
        int idIt = rs.getInt("id");
        int idKjob = rs.getInt("kitchenjob");

        KitchenJob kjob = this.innerLoadKitchenJob(idKjob);

        Section sec = null;
        if (idSec > 0) {
          //la sezione a questo punto dovrebbe essere già stata aggiunta
          sec = this.idToSectionObject.get(idSec);
        }
        MenuItem it = m.addItem(kjob, sec, description);
        this.itemObjects.put(it, idIt);
        this.idToItemObject.put(idIt, it);
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
  }

  private KitchenJob innerLoadKitchenJob(int idKjob) {
    //verifico se l'ho già caricato in precedenza
    KitchenJob kjob = this.idToKitchenJobObject.get(idKjob);
    if (kjob != null) return kjob;

    Statement st = null;

    String query = "SELECT * FROM KitchenJobs WHERE KitchenJobs.id = " + idKjob;
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      if (rs.next()) {
        String name = rs.getString("name");
        char type = rs.getString("type").charAt(0);
        kjob = createKitchenJobWithType(name, type);
        this.kitchenJobObjects.put(kjob, idKjob);
        this.idToKitchenJobObject.put(idKjob, kjob);
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }

    return kjob;
  }

  private KitchenJob createKitchenJobWithType(String name, char type) {
    switch (type) {
      case 'r':
        return new KitchenJob(name, KitchenJob.Type.Dish);
      case 'p':
        return new KitchenJob(name, KitchenJob.Type.Preparation);

    }
    return null;
  }

  private void loadMenuSections(int id, Menu m) {
    //Caricamento sezioni
    //Non verifichiamo se una Section è già stata creata perché
    //questo può avvenire solo nel contesto del caricamento di un Menu
    //e la Section può essere già creata solo se il Menu è stato creato;
    //il controllo sul Menu avviene già in loadMenus
    Statement st = null;
    String query = "SELECT Sections.* FROM Sections WHERE Sections.menu=" + id + " ORDER BY Sections.position";

    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {
        String name = rs.getString("name");
        int idSec = rs.getInt("id");

        Section sec = m.addSection(name);
        this.sectionObjects.put(sec, idSec);
        this.idToSectionObject.put(idSec, sec);
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }

  }

  private User innerLoadUser(int userId) {
    //verifico se l'ho già caricato in precedenza
    User u = this.idToUserObject.get(userId);
    if (u != null) return u;

    Statement st = null;
    String query = "SELECT Users.id, Users.name, UserRoles.role FROM Users LEFT JOIN UserRoles on Users.id = " +
      "UserRoles.user where Users.id=" + userId;
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        if(u == null) {
          u = new User(rs.getString("name"));
          this.userObjects.put(u, userId);
          this.idToUserObject.put(userId, u);
        }
        addUserRole(u, rs);
      }

    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }

    return u;
  }

  private Menu innerLoadMenu(int menuId) {
    Menu m = this.idToMenuObject.get(menuId);
    if (m != null) 
      return m;
    Statement st = null;
    String query = "SELECT * FROM Menus WHERE id = li" + menuId;

    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");

        //Verifica se per caso l'ha già caricato
        if (m == null) {

          String title = rs.getString("title");
          int ownerid = rs.getInt("menuowner");
          User owner = this.innerLoadUser(ownerid);

          m = new Menu(owner, title);
          m.setPublished(rs.getBoolean("published"));
          m.setBuffet(rs.getBoolean("buffet"));
          m.setCookRequired(rs.getBoolean("cookRequired"));
          m.setFingerFood(rs.getBoolean("fingerFood"));
          m.setHotDishes(rs.getBoolean("hotDishes"));
          m.setKitchenRequired(rs.getBoolean("kitchenRequired"));

          //per sapere se il menu è in uso consulto la tabella degli eventi
          //NdR: un menu è in uso anche se l'evento che lo usa è concluso o annullato
          Statement st2 = this.connection.createStatement();
          String query2 = "SELECT Events.id FROM Events JOIN Menus M on Events.menu = M.id WHERE M.id=" + id;
          ResultSet rs2 = st2.executeQuery(query2);
          m.setInUse(rs2.next());
          st2.close();
          loadMenuSections(id, m);
          loadMenuItems(id, m);
          this.menuObjects.put(m, id);
          this.idToMenuObject.put(id, m);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return m;
  }

  private void addUserRole(User u, ResultSet rs) throws SQLException {
    char roleName = rs.getString("role").charAt(0);
    switch (roleName) {
      case 'c':
        u.addRole(User.Role.Cuoco);
        break;
      case 'h':
        u.addRole(User.Role.Chef);
        break;
      case 'o':
        u.addRole(User.Role.Organizzatore);
        break;
      case 's':
        u.addRole(User.Role.Servizio);
        break;
    }
  }

  private Event innerLoadEvent(int eid) {
    //Verifica se per caso l'ha già caricata
    Event event = this.idToEventObject.get(eid);
    if(event != null)
      return event;
    Statement st = null;
    String query = "SELECT * FROM Events WHERE id ="+eid;
    try {
      st = this.connection.createStatement();
      ResultSet rs = st.executeQuery(query);
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String location = rs.getString("location");
        LocalDate date = LocalDate.parse(rs.getString("date"));
        int numberOfParticipants = rs.getInt("numberOfParticipants");
        String serviceType = rs.getString("serviceType");
        String tipologyNote = rs.getString("tipologyNote");
        String annotation = rs.getString("annotation");
        boolean closed = rs.getBoolean("closed");
        User organiser = innerLoadUser(rs.getInt("organiser"));
        User assignedChef = innerLoadUser(rs.getInt("assignedChef"));
        Menu menu = innerLoadMenu(rs.getInt("menu"));

        if (event == null) 
          event = new Event(name, location, date, numberOfParticipants,
              serviceType, tipologyNote, annotation, closed, organiser,
              assignedChef, menu);

        if (event != null) {
          this.eventObjects.put(event, id);
          this.idToEventObject.put(id, event);
        }
      }
    } catch (SQLException exc) {
      exc.printStackTrace();
    } finally {
      try {
        if (st != null) st.close();
      } catch (SQLException exc2) {
        exc2.printStackTrace();
      }
    }
    return event;
  }
}

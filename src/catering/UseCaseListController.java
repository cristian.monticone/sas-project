package catering;

import catering.businesslogic.CateringAppManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class UseCaseListController {

    @FXML
    private BorderPane mainPane;

    @FXML
    private BorderPane useCaseListPane;

    @FXML
    Button menuButton;

    @FXML
    Button tasksButton;

    private MenuListController menuListController;
    private BorderPane menuListPane;

    private SheetListController sheetListController;
    private BorderPane sheetListPane;

    @FXML
    public void initialize() {
        try {
            FXMLLoader menuListLoader = new FXMLLoader(getClass().getResource("menulist.fxml"));
            menuListPane = menuListLoader.load();
            menuListController = menuListLoader.getController();

            menuListController.listen(( () -> {
              mainPane.setCenter(useCaseListPane);
            }));


            FXMLLoader sheetListLoader = new FXMLLoader(getClass().getResource("sheetlist.fxml"));
            sheetListPane = sheetListLoader.load();
            sheetListController = sheetListLoader.getController();

            sheetListController.listen(( () -> {
              mainPane.setCenter(useCaseListPane);
            }));

        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    @FXML
    private void menuUseCaseAction() {
      mainPane.setCenter(menuListPane);
    }

    @FXML
    private void tasksUseCaseAction() {
      mainPane.setCenter(sheetListPane);
    }
}

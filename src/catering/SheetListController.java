package catering;

import catering.businesslogic.CateringAppManager;
import catering.businesslogic.Sheet;
import catering.businesslogic.Event;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

public class SheetListController {
    private List<Sheet> sheets;
    private ObservableList<Sheet> observableSheets;
    private Sheet selectedSheet;

    private SheetEditController sheetEditController;

    @FXML
    private BorderPane mainContainer;

    @FXML
    Button editSheetButton;

    @FXML
    private BorderPane sheetListPane;

    @FXML
    private List<SheetListListener> sheetListListeners;

    private BorderPane sheetEditPane;

    @FXML
    private ListView<Sheet> sheetList;

    @FXML
    public void initialize() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("sheetedit.fxml"));
            sheetEditPane = loader.load();
            sheetEditController = loader.getController();
            sheetEditController.listen(( () -> {
                this.resetSheetList();
                mainContainer.setCenter(sheetListPane);
            }));

        } catch (IOException exc) {
            exc.printStackTrace();
        }

        sheetList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        resetSheetList();

        sheetList.getSelectionModel().selectedIndexProperty().addListener((observable) -> {
            selectedSheet = sheetList.getSelectionModel().getSelectedItem();

            Event e = null;
            if (selectedSheet != null)
              e = selectedSheet.getEvent();

            boolean ownershipOk = (selectedSheet != null) && (e.getAssignedChef().equals(CateringAppManager.userManager.getCurrentUser()));

            editSheetButton.setDisable(!ownershipOk);
        });
    }

    public SheetListController() {
      sheetListListeners = new ArrayList<>();

    } 

    public void listen(SheetListListener l) {
        sheetListListeners.add(l);
    }

    @FXML
    private void goHomeAction() {
        for (SheetListListener l: sheetListListeners) {
            l.onGoHome();
        }
    }

    private void resetSheetList() {
        sheets = CateringAppManager.sheetManager.getAllSheets();
        observableSheets = FXCollections.observableList(sheets);
        sheetList.setItems(observableSheets);
    }

    @FXML
    private void newSheetAction() {
        List<Event> choices = CateringAppManager.eventManager.getAllEvents();

        /* We need only the events not associeted whit a sheet. */
        List<Sheet> sheets = CateringAppManager.sheetManager.getAllSheets();
        for (Sheet s: sheets)
            choices.remove(s.getEvent());

        /* Showing only self assigned events. */
        for (Event e: CateringAppManager.eventManager.getAllEvents())
            if (e.getAssignedChef() != CateringAppManager.userManager.getCurrentUser())
              choices.remove(e);

        if (choices.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Avvertenza");
            alert.setHeaderText(null);
            alert.setContentText("Non ci sono eventi assegnati all'utente " +
              CateringAppManager.userManager.getCurrentUser() + " senza " +
              "foglio riepilogativo.");
            alert.showAndWait();
            return;
        }

        ChoiceDialog<Event> dialog = new ChoiceDialog<>(choices.get(0), choices);
        dialog.setTitle("Evento");
        dialog.setHeaderText("Evento");
        dialog.setContentText("Scegli un evento:");

        Optional<Event> result = dialog.showAndWait();

        if (result.isPresent()) {
          CateringAppManager.sheetManager.createSheet(result.get());

          result.ifPresent(title -> {
              sheetEditController.setUp();
              mainContainer.setCenter(sheetEditPane);
          });
        }
    }

    @FXML
    private void editSheetAction() {
          CateringAppManager.sheetManager.selectSheet(selectedSheet);

          sheetEditController.setUp();
          mainContainer.setCenter(sheetEditPane);
    }

}

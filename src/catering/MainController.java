package catering;

import catering.businesslogic.CateringAppManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class MainController {

    @FXML
    private BorderPane mainPane;

    @FXML
    private Label userName;

    @FXML
    public void initialize() {
        userName.setText(CateringAppManager.userManager.getCurrentUser().toString());

        try {
            FXMLLoader useCaseListLoader = new FXMLLoader(getClass().getResource("usecaselist.fxml"));
            Parent useCaseList = useCaseListLoader.load();
            UseCaseListController useCaseListController = useCaseListLoader.getController();

            mainPane.setCenter(useCaseList);
        } catch (IOException exc) {
            exc.printStackTrace();
        }

    }
}

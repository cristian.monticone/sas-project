package catering;

import catering.businesslogic.*;
import catering.businesslogic.Menu;
import catering.businesslogic.MenuItem;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class SheetEditController {
    private ObservableList<Activity> observableList;
    private ObservableList<Task> observableTaskList;
    private List<Activity> activities;
    private Activity selectedActivity;
    private Task selectedTask;

    @FXML
    private List<SheetEditListener> editListeners;

    @FXML
    private ListView<Activity> activityList;

    @FXML
    private ListView<Task> taskList;

    @FXML
    private Button removeActivity;

    @FXML
    private Button upButton;

    @FXML
    private Button downButton;

    @FXML
    private Button editButton;

    @FXML
    private Button removeButton;

    @FXML
    private Button addTaskButton;

    @FXML
    public void initialize() {
        activityList.getSelectionModel().selectedIndexProperty().addListener((observable) -> {
            selectedActivity = activityList.getSelectionModel().getSelectedItem();

            updateTaskButtons();

            if (selectedActivity == null) {
                //if not selected no one
                removeActivity.setDisable(true);
                upButton.setDisable(true);
                downButton.setDisable(true);
            }else if(activities.size() == 1){
                removeActivity.setDisable(false);
                upButton.setDisable(true);
                downButton.setDisable(true);
            }else if (activities.indexOf(selectedActivity) == 0) {
                //if selected the first
                removeActivity.setDisable(false);
                upButton.setDisable(true);
                downButton.setDisable(false);
            }else if(activities.indexOf(selectedActivity) == activities.size() -1){
                //if selected the last one
                removeActivity.setDisable(false);
                upButton.setDisable(false);
                downButton.setDisable(true);
            }else {
                //any other cases
                removeActivity.setDisable(false);
                upButton.setDisable(false);
                downButton.setDisable(false);
            }

            this.resetTaskList();
        });

        taskList.getSelectionModel().selectedIndexProperty().addListener((observable) -> {
            updateTaskButtons();
            this.resetTaskList();
        });
    }

    public void updateTaskButtons() {
        selectedTask = taskList.getSelectionModel().getSelectedItem();

        if (selectedActivity == null)
          addTaskButton.setDisable(true);
        else
          addTaskButton.setDisable(false);

        if (selectedTask == null) {
          removeButton.setDisable(true);
          editButton.setDisable(true);
        }else {
            removeButton.setDisable(false);
            editButton.setDisable(false);
        }
    }

    public void setUp() {
        resetKitchenList();
    }

    public SheetEditController() {
        editListeners = new ArrayList<>();
    }

    public  void listen(SheetEditListener l) {
        editListeners.add(l);
    }

    @FXML
    private void onCloseSheet() {
        for (SheetEditListener l: editListeners) {
            l.onClose();
        }
    }

    private void resetKitchenList() {
        activities = CateringAppManager.sheetManager.getCurrentSheet().getActivities();
        observableList = FXCollections.observableList(activities);
        activityList.setItems(observableList);
    }

    private void resetTaskList() {
        List<Task> tasks = (selectedActivity != null) ? selectedActivity.getTasks() : new ArrayList<Task>();
        observableTaskList = FXCollections.observableList(tasks);
        taskList.setItems(observableTaskList);
    }

    @FXML
    private void onAddActivity() {
        List<KitchenJob> choices = CateringAppManager.kitchenJobManager.getKitchenJobs();

        /* We need only the activity not associeted whit a sheet. */
        List<Activity> activities = CateringAppManager.sheetManager.getCurrentSheet().getActivities();

        for (Activity s: activities)
            choices.remove(s.getKitchenJob());

        if (choices.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Avvertenza");
            alert.setHeaderText(null);
            alert.setContentText("Non ci sono attività da aggiungere");
            alert.showAndWait();
            return;
        }

        ChoiceDialog<KitchenJob> dialog = new ChoiceDialog<>(choices.get(0), choices);
        dialog.setTitle("Attività");
        dialog.setHeaderText("Attività");
        dialog.setContentText("Scegli attività:");

        Optional<KitchenJob> result = dialog.showAndWait();
        result.ifPresent(title -> {

            CateringAppManager.sheetManager.addActivity(result.get());
            resetKitchenList();
            resetTaskList();
        });
    }

    @FXML
    private void onAssignTask(ActionEvent e) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("addTaskFXML.fxml"));
        
        Parent parent = null;

        try {
            parent = fxmlLoader.load();
            fxmlLoader.<AddTaskController>getController().setSelectedActivity(selectedActivity);
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setTitle("Compito");
            stage.setScene(scene);
            stage.showAndWait();
            this.resetKitchenList();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void onTaskEdit() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("addTaskFXML.fxml"));
        
        Parent parent = null;

        try {
            parent = fxmlLoader.load();
            fxmlLoader.<AddTaskController>getController().setSelectedActivity(selectedActivity);
            fxmlLoader.<AddTaskController>getController().setSelectedTask(selectedTask);
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setTitle("Compito");
            stage.setScene(scene);
            stage.showAndWait();
            this.resetKitchenList();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void onRemoveTask() {
        CateringAppManager.sheetManager.removeTask(selectedActivity, selectedTask);
        resetKitchenList();
        resetTaskList();
    }

    @FXML
    private void removeActivity(){
        CateringAppManager.sheetManager.removeActivity(selectedActivity);
        resetKitchenList();
        resetTaskList();
    }

    @FXML
    private void orderActivityUp(){
        if(activities.indexOf(selectedActivity) != 0) {
            CateringAppManager.sheetManager.orderActivity(selectedActivity, activities.indexOf(selectedActivity) - 1);
            resetKitchenList();
        }
    }

    @FXML
    private void orderActivityDown(){
        if(activities.indexOf(selectedActivity) != activities.size() -1) {
            CateringAppManager.sheetManager.orderActivity(selectedActivity, activities.indexOf(selectedActivity) + 1);
            resetKitchenList();
        }
    }
}

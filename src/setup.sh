#!/bin/sh

BASE_DIR="$(git rev-parse --show-toplevel)"

RIGHTSHA256_JFX="8de2c84a5844341d140074f5070deca1f7865733ef0176a8114540a9db2e4657  openjfx-12.0.1_linux-x64_bin-sdk.zip"
RIGHTSHA256_JDBC="de7d7f8ce9d777c2a6c7c47695d87bfd635e621f9142467a5e56538fcaf4644a  mysql-connector-java-8.0.16.tar.gz"

wget "https://download2.gluonhq.com/openjfx/12.0.1/openjfx-12.0.1_linux-x64_bin-sdk.zip"
wget "https://cdn.mysql.com//Downloads/Connector-J/mysql-connector-java-8.0.16.tar.gz"

FILENAME_JFX="openjfx-12.0.1_linux-x64_bin-sdk.zip"
FILENAME_JDBC="mysql-connector-java-8.0.16.tar.gz"

REMOTESHA256_JFX=$(sha256sum $FILENAME_JFX)
REMOTESHA256_JDBC=$(sha256sum $FILENAME_JDBC)

if [ "$REMOTESHA256_JFX" != "$RIGHTSHA256_JFX" ]; then
  echo "$FILENAME_JFX checksum failed!" >&2
  exit 1
fi

if [ "$REMOTESHA256_JDBC" != "$RIGHTSHA256_JDBC" ]; then
  echo "$FILENAME_JDBC checksum failed!" >&2
  exit 1
fi

unzip $FILENAME_JFX > /dev/null
tar -xvf $FILENAME_JDBC > /dev/null

mkdir libs 2> /dev/null

mv javafx-sdk-12.0.1/lib/*.jar libs/
mv mysql-connector-java-8.0.16/mysql-connector-java-8.0.16.jar libs/

rm -rf javafx-sdk-12.0.1/
rm -rf mysql-connector-java-8.0.16/

rm $FILENAME_JFX
rm $FILENAME_JDBC

echo "Setup completed."

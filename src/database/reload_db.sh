#!/bin/sh

DBNAME="catering"
USERNAME="root"

QUERY="DROP DATABASE $DBNAME;"
mysql -u $USERNAME -e "$QUERY"

QUERY="CREATE DATABASE $DBNAME;"
mysql -u $USERNAME -e "$QUERY"

mysql -u $USERNAME $DBNAME < db_schema.sql
mysql -u $USERNAME $DBNAME < db_data.sql

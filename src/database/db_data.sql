--
-- Database: catering
--

-- --------------------------------------------------------

--
-- Dumping data for table KitchenJobs
--

INSERT INTO KitchenJobs (id, name, type) VALUES
(1, 'Salsa Tonnata', 'p'),
(2, 'Vitello Tonnato', 'r'),
(3, 'Vitello Tonnato all\'Antica', 'r'),
(4, 'Brodo di Manzo Ristretto', 'p'),
(5, 'Risotto alla Milanese', 'r'),
(6, 'Pesto Ligure', 'p'),
(7, 'Trofie avvantaggiate al pesto', 'r'),
(8, 'Orata al forno con olive', 'r'),
(9, 'Insalata russa', 'r'),
(10, 'Bagnet vert', 'p'),
(11, 'Acciughe al verde', 'r'),
(12, 'Agnolotti del plin', 'p'),
(13, 'Agnolotti al sugo d\'arrosto', 'r'),
(14, 'Agnolotti burro e salvia', 'r'),
(15, 'Brasato al barolo', 'r'),
(16, 'Panna cotta', 'r'),
(17, 'Tarte tatin', 'r');

-- --------------------------------------------------------

--
-- Dumping data for table Users
--

INSERT INTO Users (id, name) VALUES
(1, '[nessuno]'),
(2, 'Tony'),
(3, 'Viola'),
(4, 'Anna'),
(5, 'Giovanni');

-- --------------------------------------------------------

--
-- Dumping data for table Roles
--

INSERT INTO Roles (id, role) VALUES
('c', 'Cuoco'),
('h', 'Chef'),
('o', 'Organizzatore'),
('s', 'Servizio');

-- --------------------------------------------------------

--
-- Dumping data for table UserRoles
--

INSERT INTO UserRoles (user, role) VALUES
(1, 'c'),
(2, 'h'),
(2, 'c'),
(3, 'h'),
(4, 'o'),
(4, 'h'),
(5, 'c');

-- --------------------------------------------------------

--
-- Dumping data for table Menus
--

INSERT INTO Menus (id, title, menuowner, published, fingerFood, cookRequired, hotDishes, kitchenRequired, buffet) VALUES
(1, 'prova in uso', 3, 4, 1, 0, 0, 0, 1),
(2, 'prova non in uso', 2, 0, 0, 1, 1, 1, 0),
(3, 'prova struttura', 3, 0, 0, 0, 0, 0, 1),
(4, 'Ciao', 3, 0, 0, 0, 0, 0, 0),
(7, 'prova struttura', 3, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Dumping data for table Events
--

INSERT INTO Events (name, menu, organiser, assignedChef, location, date, 
  numberOfParticipants, serviceType, tipologyNote, annotation, closed) VALUES
('Evento 1',1, 4, 2,'Torino','2019-06-10',120,'tipo_servizio','note_tipologia','annotazioni',0),
('Evento 2',1, 4, 3,'Torino','2019-06-10',120,'tipo_servizio','note_tipologia','annotazioni',0),
('Evento 3',1, 4, 4,'Torino','2019-06-10',120,'tipo_servizio','note_tipologia','annotazioni',0),
('Evento 4',1, 4, 3,'Torino','2019-06-10',120,'tipo_servizio','note_tipologia','annotazioni',0),
('Evento 5', 1, 4, 2,'Torino','2019-06-10',120,'tipo_servizio','note_tipologia','annotazioni',0),
('Evento 6', 1, 4, 3,'Torino','2019-06-10',120,'tipo_servizio','note_tipologia','annotazioni',0);

-- --------------------------------------------------------

--
-- Dumping data for table Sections
--

INSERT INTO Sections (menu, id, name, position) VALUES
(3, 1, 'Primi', NULL),
(3, 2, 'Secondi', NULL),
(3, 3, 'Dessert', NULL);

-- --------------------------------------------------------

--
-- Dumping data for table MenuItems
--

INSERT INTO MenuItems (id, menu, section, description, kitchenjob, position) VALUES
(1, 3, 1, 'Voce 1', 1, 0),
(2, 3, 1, 'Voce 2', 1, 1),
(3, 3, 2, 'Voce 3', 1, 0),
(4, 3, 2, 'Voce 4', 1, 2),
(5, 3, 0, 'Voce 0', 1, 0),
(6, 3, 1, 'Voce 1.5', 1, 2),
(7, 3, 2, 'Voce 3.5', 1, 1),
(8, 3, 0, 'Voce 0.2', 1, 1),
(9, 3, 0, 'Voce 0.8', 1, 2),
(10, 3, 3, 'Voce 5', 1, 0),
(11, 3, 3, 'Voce 6', 1, 1),
(12, 3, 3, 'Voce 7', 1, 2);

-- --------------------------------------------------------

--
-- Dumping data for table Shifts
--

INSERT INTO Shifts(day, startTime, endTime, inPlace, complete) VALUES
('2019-06-01','00:00','01:00','0','0'),
('2019-06-02','01:00','02:00','0','0'),
('2019-06-03','02:00','03:00','0','0'),
('2019-06-04','03:00','04:00','0','0'),
('2019-06-05','04:00','05:00','0','0'),
('2019-06-06','05:00','06:00','0','0'),
('2019-06-07','06:00','07:00','0','0'),
('2019-06-08','07:00','08:00','0','0'),
('2019-06-09','08:00','09:00','0','0'),
('2019-06-10','09:00','10:00','0','0');

-- --------------------------------------------------------

--
-- Dumping data for table Sheets
--

INSERT INTO Sheets(event) VALUES
(1),
(3),
(4);

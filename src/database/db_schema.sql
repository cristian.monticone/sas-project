--
-- Database: catering
--

-- --------------------------------------------------------

--
-- Table structure for table Roles
--

CREATE TABLE Roles (
  id varchar(1),
  role varchar(32) DEFAULT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Users
--

CREATE TABLE Users (
  id int(11) AUTO_INCREMENT,
  name varchar(128) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table UserRoles
--

CREATE TABLE UserRoles (
  user int(11),
  role varchar(1),
  PRIMARY KEY (user, role),
  FOREIGN KEY (user) REFERENCES Users(id),
  FOREIGN KEY (role) REFERENCES Roles(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Menus
--

CREATE TABLE Menus (
  id int(11) AUTO_INCREMENT,
  title tinytext NOT NULL,
  menuowner int(11) NOT NULL,
  published tinyint(1) DEFAULT NULL,
  fingerFood tinyint(1) DEFAULT NULL,
  cookRequired tinyint(1) DEFAULT NULL,
  hotDishes tinyint(1) DEFAULT NULL,
  kitchenRequired tinyint(1) DEFAULT NULL,
  buffet tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (menuowner) REFERENCES Users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table KitchenJobs
--

CREATE TABLE KitchenJobs (
  id int(11) AUTO_INCREMENT,
  name varchar(128) DEFAULT NULL,
  type varchar(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Events
--

CREATE TABLE Events (
  id int(11) AUTO_INCREMENT,
  menu int(11) NOT NULL,
  organiser int(11) NOT NULL,
  assignedChef int(11) NOT NULL,
  name tinytext DEFAULT NULL,
  location tinytext DEFAULT NULL,
  date tinytext DEFAULT NULL,
  numberOfParticipants int(11) DEFAULT NULL,
  serviceType tinytext DEFAULT NULL,
  tipologyNote tinytext DEFAULT NULL,
  annotation tinytext DEFAULT NULL,
  closed boolean DEFAULT '0',
  PRIMARY KEY (id),
  FOREIGN KEY (menu) REFERENCES Menus(id),
  FOREIGN KEY (organiser) REFERENCES Users(id),
  FOREIGN KEY (assignedChef) REFERENCES Users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Sections
--

CREATE TABLE Sections (
  id int(11) AUTO_INCREMENT,
  menu int(11) NOT NULL,
  name tinytext,
  position int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (menu) REFERENCES Menus(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table MenuItems
--

CREATE TABLE MenuItems (
  id int(11) AUTO_INCREMENT,
  menu int(11) DEFAULT NULL,
  section int(11) DEFAULT '0',
  description tinytext,
  kitchenjob int(11) NOT NULL,
  position int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (menu) REFERENCES Menus(id),
--  FOREIGN KEY (section) REFERENCES Sections(id), 
--      TODO: (the no section can't be reffered with this constraint)
  FOREIGN KEY (kitchenjob) REFERENCES KitchenJobs(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Sections
--

CREATE TABLE Shifts (
  id int(11) AUTO_INCREMENT,
  day tinytext,
  startTime tinytext DEFAULT NULL,
  endTime tinytext DEFAULT NULL,
  inPlace boolean DEFAULT '0',
  complete boolean DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Sheets 
--

CREATE TABLE Sheets (
  id int(11) AUTO_INCREMENT,
  event int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (event) REFERENCES Events(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Activities
--

CREATE TABLE Activities (
  id int(11) AUTO_INCREMENT,
  kitchenjob int(11) NOT NULL,
  sheet int(11) NOT NULL,
  position int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (kitchenjob) REFERENCES KitchenJobs(id),
  FOREIGN KEY (sheet) REFERENCES Sheets(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table Tasks
--

CREATE TABLE Tasks (
  id int(11) AUTO_INCREMENT,
  activity int(11) NOT NULL,
  shift int(11) NOT NULL,
  cook int(11) NOT NULL,
  time tinytext DEFAULT NULL,
  quantity REAL DEFAULT '0',
  toBePrepared BOOLEAN DEFAULT '0',
  completed BOOLEAN DEFAULT '0',
  PRIMARY KEY (id),
  FOREIGN KEY (activity) REFERENCES Activities(id),
  FOREIGN KEY (cook) REFERENCES Users(id),
  FOREIGN KEY (shift) REFERENCES Shifts(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

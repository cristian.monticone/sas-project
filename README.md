# SaS 2018/19 Project

This git repository contains **_awesome_** documentation for our SaS project.

As settled, we'll **NOT** discuss this project in time for the Diego's degree.

## Repository contents
The main _Domain Model_ is directly insede the root directory of the repo.

"ricette" folder contains the _SSD_ and _DSDs_ about the **GestireRicette** UC.

Also applies to "eventi" and "compiti" folders respectively to the UCs of **GestireEventi** and **AssegnaCompiti**.

Implemented codebase is collocated in the _src_ dir.

## Usefull pointers
- [CAT & RING Project document](https://docs.google.com/document/d/1AClV0x9Rf6z0FIJUkUFTUTyNMNsPU_hho_4cjxkZLR4)
- [Actors and UC titles](https://docs.google.com/document/d/1ju9G6tCSey61hFvSns2z10sZBVfvv2hrsb3d8KlkP0U)
- [Initial glossary](https://docs.google.com/document/d/1VskCHNOzOHvi8MV1POWOiB7RUCJ0fRy7MMRv-I-H7oM)
- [User stories about "Gestire eventi" and "Gestire ricette" UCs](https://docs.google.com/document/d/12fN4LObZTnLJzeJpwP1WF9jk3I4-sx6ioMYE6sS5Tr0)

## TODOs
- ### "Assegna compiti" UC
    - [x] Merge UC?
    - [x] Merge SSD
    - [x] Merge domain models
    - [x] Merge Contracts
    - [x] Freeze "Assegna compiti" UC
    - [x] DSDs and DCD
    - [x] Development
    - [x] Final review
- ### "Gestire eventi" UC
    - [x] Detailed UC
    - [x] SSD
    - [x] Domain model
    - [x] Contracts
    - [x] DSDs and DCD
    - [x] Final review

- ### "Gestire ricette" UC
    - [x] Detailed UC
    - [x] SSD
    - [x] Domain model
    - [x] Contracts
    - [x] DSDs and DCD
    - [x] Final review

- ### Happy Ending
    - [x] Draft documents
    - [x] Documents revision
    - [x] Submit project
    - [x] Graduate :-)

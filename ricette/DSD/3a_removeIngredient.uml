@startuml
title 3a removeIngredient
actor Actor
participant "CatERingAppManager.kitchenJobManager:KitchenJobManager" as kjm
participant "kitchenJobManager.currentKitchenJob: KitchenJob" as kj
participant "dosages: List<Dosage>" as dl
participant "r: KitchenJobReceiver" as rec

Actor -> kjm++: removeIngredient(ingredient)

alt currentKitchenJob == null 
    kjm -->> Actor: throw UseCaseLogicException
else !currentKitchenJob.hasIngredient(ingredient)
    kjm -->> Actor: throw KitchenJobException
else
    kjm -> kj++: getDosage(ingredient)
    kjm <<-- kj--: dosage

    kjm -> kj++: removeDosage(dosage)
      kj -> dl++: remove(dosage)
      kj <<-- dl--
    kjm <<-- kj--

    group for each r in receivers
    	kjm -> rec++:notifyDosageRemoved(currentKitchenJob, dosage)
        deactivate rec
    end 
    
    kjm -->> Actor--: dosage
    
end
@enduml

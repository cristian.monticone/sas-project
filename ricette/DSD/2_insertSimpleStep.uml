@startuml
title 2 insertSimpleStep
actor Actor
participant "CatERingAppManager.kitchenJobManager:KitchenJobManager" as kjm
participant "simpleStep: SimpleStep" as sims
participant "parentGroup: GroupStep" as gros
participant "childrens: List<GenericStep>" as childs
participant "requiredIngredients: List<Ingredient>" as ings
participant "r: KitchenJobReceiver" as rec

Actor -> kjm++: insertSimpleStep(description, anticipation, repetitions, parentGroup)

alt currentKitchenJob == null 
    kjm -->> Actor: throw UseCaseLogicException
else !currentKitchenJob.hasStep(parentGroup)
    kjm -->> Actor: throw UseCaseLogicException
else
    kjm --> sims**: createSimpleStep(description, anticipation, repetitions)
      activate sims
      sims --> ings**: create
      sims -> sims: setContent(description)
      sims -> sims: setAnticipation(anticipation)
      sims -> sims: setRepetitions(repetitions)
      sims -->> kjm --: simpleStep

    kjm -> gros++: addChildren(simpleStep)
      gros -> childs++: add(simpleStep)
      childs -->> gros--
      gros -->> kjm--

   
    group for each r in receivers
    	kjm -> rec++:notifySimpleStepAdded(currentKitchenJob, simpleStep)
        deactivate rec
    end 
    
    kjm -->> Actor--: simpleStep
    
end
@enduml

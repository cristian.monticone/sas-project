@startuml
title 3 insertIngredient
actor Actor
participant "CatERingAppManager.kitchenJobManager:KitchenJobManager" as kjm
participant "kitchenJobManager.currentKitchenJob: KitchenJob" as kj
participant "dosage: Dosage" as dos
participant "currentKitchenJob.ingredients: List<Dosage>" as il
participant "r: KitchenJobReceiver" as rec

Actor -> kjm++: insertIngredient(ingredient, quantity, measureUnit?)

alt currentKitchenJob == null 
    kjm -->> Actor: throw UseCaseLogicException
else currentKitchenJob.hasIngredient(ingredient)
    kjm -->> Actor: throw KitchenJobException
else
    kjm -> kj++: addDosage(ingredient, quantity, measureUnit)
      kj --> dos**:create(quantity, measureUnit, ingredient)
        activate dos
        dos -> dos: setQuantity(quantity)
        dos -> dos: setMeasueUnit(measureUnit)
        dos -> dos: setIngredient(ingredient)
        kj <<-- dos--: dosage
      kj -> il++: add(dosage)
      kj <<-- il--
    kjm <<-- kj--: dosage

    group for each r in receivers
    	kjm -> rec++:notifyIngredientAdded(currentKitchenJob, dosage)
        deactivate rec
    end 
    
    kjm -->> Actor--: dosage
    
end
@enduml
